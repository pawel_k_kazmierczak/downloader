<?php

namespace App\Form;

class Link {

    protected $url;

    public function getUrl() {
	return $this->url;
    }

    public function setUrl($url) {
	$this->url = $url;
	return $this;
    }

}
