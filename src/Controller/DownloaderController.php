<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Service\DownloaderService;
use App\Form\Link;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DownloaderController extends Controller {

    /**
     * @Route("/downloader", name="downloader")
     */
    public function index(DownloaderService $downloaderService, Request $request) {

	$link = new Link();

	$form = $this->createFormBuilder($link)
		->add('url', TextType::class)
		->add('save', SubmitType::class, array('label' => 'Zapisz'))
		->getForm();

	$form->handleRequest($request);

	$response = false;
	if ($form->isSubmitted() && $form->isValid()) {

	    $link = $form->getData();
	    $response = $downloaderService->downloadFile($link->getUrl());
	}

	$files = $downloaderService->getFiles();

	return $this->render('downloader/index.html.twig',
			[
				'form' => $form->createView(),
				'files' => $files,
				'response' => $response
	]);
    }

}
