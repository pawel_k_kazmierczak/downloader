<?php

namespace App\Service;

class DownloadResponse {

    public $success = true;
    public $msg = "Dodano plik";

    public function setSuccess($success) {
	$this->success = $success;
	return $this;
    }

    public function setMsg($msg) {
	$this->msg = $msg;
	return $this;
    }

}
