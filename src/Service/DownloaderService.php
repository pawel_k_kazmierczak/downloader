<?php

namespace App\Service;

use App\Entity\Files;

class DownloaderService {

    private $doctrine;
    private $em;
    private $tempDir = "tmp";
    private $uploadDir = "upload";
    private $entity = Files::class;

    public function __construct(\Doctrine\Common\Persistence\ManagerRegistry $doctrine) {
	$this->doctrine = $doctrine;
	$this->em = $this->doctrine->getManager();
    }

    public function save(Files $file) {
	$this->em->persist($file);
	$this->em->flush();
    }

    public function downloadFile($url) {
	$response = new DownloadResponse();
	try {
	    $this->downloadFileTry($url);
	} catch (\Exception $e) {
	    $response->setMsg($e->getMessage())
		    ->setSuccess(false);
	}
	$this->removeTmp();
	return $response;
    }

    private function downloadFileTry($url) {
	$url = $this->removeGetParameters($url);
	$this->tmpPath = $this->saveFileToTmp($url);
	$this->checkFileExist();
	$this->checkIsImage();

	$file = $this->createRekord($url, $this->tmpPath);
	$previousFile = $this->getLastRecordForLink($url);
	$this->checkFileHasBeenChanged($previousFile, $file);

	$this->setPathAndVersion($previousFile, $file);
	$this->copyFileToUploadDir($file);
	$this->save($file);
    }

    public function saveFileToTmp($url) {
	$filename = md5($url);

	$rozszerzenie = pathinfo($url, PATHINFO_EXTENSION);
	$tmpPath = $this->tempDir . "/$filename.$rozszerzenie";
	copy($url, $tmpPath);

	return $tmpPath;
    }

    public function createRekord($url, $tmpPath) {
	$file = new Files();

	$file->setLink($this->removeScheme($url));
	$file->setName(basename($url));
	$file->setContentHash(md5(file_get_contents($tmpPath)));
	$file->setSize(filesize($tmpPath));

	return $file;
    }

    public function getLastRecordForLink($url) {

	$link = $this->removeScheme($url);
	$query = $this->em->createQuery("SELECT e FROM {$this->entity} e where e.link = '$link' order by e.version DESC");
	$query->setMaxResults(1);
	$files = $query->getResult();

	if (count($files) > 0) {
	    return $files[0];
	} else {
	    $result = new Files();
	    $result->setVersion(0);
	    return $result;
	}
    }

    public function getFiles() {
	$qb = $this->em->createQueryBuilder();
	$qb->select(array('e'))
		->from($this->entity, 'e')
		->orderBy('e.version', 'DESC')
		->groupBy('e.link')
	;
	return $qb->getQuery()->getResult();
    }

    public function hasBeenChanged(Files $file1, Files $file2) {
	return $file1->getContentHash() !== $file2->getContentHash();
    }

    public function setPathAndVersion(Files $prev, Files $file) {
	$file->setVersion($prev->getVersion() + 1);

	$path = $this->uploadDir . "/" . $file->getVersion() . "_" . $this->removeSlash($file->getLink());
	$file->setPath($path);
    }

    public function copyFileToUploadDir(Files $file) {
	copy($this->tmpPath, $file->getPath());
    }

    public function removeTmp() {
	if (isset($this->tmpPath) && file_exists($this->tmpPath)) {
	    unlink($this->tmpPath);
	}
    }

    function setTempDir($tempDir) {
	$this->tempDir = $tempDir;
	return $this;
    }

    function setUploadDir($uploadDir) {
	$this->uploadDir = $uploadDir;
	return $this;
    }

    private function checkFileExist() {
	if (filesize($this->tmpPath) == 0) {
	    throw new \Exception("Plik ma 0B i prawdopodobnie nie istnieje.");
	}
    }

    private function checkIsImage() {
	$finfo = finfo_open(FILEINFO_MIME_TYPE);
	$type = finfo_file($finfo, $this->tmpPath);

	if (strpos($type, 'image') === false) {
	    throw new \Exception("Plik nie jest obrazkiem");
	}
    }

    private function checkFileHasBeenChanged($previousFile, $file) {
	if (!$this->hasBeenChanged($previousFile, $file)) {
	    throw new \Exception("Plik nie uległ zmianie");
	}
    }

    private function removeGetParameters($url) {
	return strtok($url, '?');
    }

    private function removeScheme($url) {
	$url = str_replace('https://', '', $url);
	$url = str_replace('http://', '', $url);
	return $url;
    }

    private function removeSlash($path) {
	$path = str_replace('/', '_', $path);
	$path = str_replace('\\', '_', $path);
	return $path;
    }

}
