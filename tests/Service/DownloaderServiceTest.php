<?php

namespace App\Tests\Service;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Entity\Files;
use App\Service\DownloaderService;

class DownloaderServiceTest extends KernelTestCase {

    private $entityManager;
    private $downloaderService;
    private $testDir = "public/test";

    protected function setUp() {
	$kernel = self::bootKernel();

	$this->doctrine = $kernel->getContainer()
		->get('doctrine');
	$this->entityManager = $this->doctrine->getManager();

	$this->downloaderService = new DownloaderService($this->doctrine);

	$this->setTestDirs();
    }

    public function test_save() {
	$file = new Files();
	$file->setLink(__METHOD__);

	$this->downloaderService->save($file);

	$this->assertNotNull($file->getId());
    }

    public function test_downloadFile() {
	$this->createDownloaderMock();
	$this->setTestDirs();

	$result = $this->downloaderService->downloadFile('http://pastmo.pl/logo.jpg');

	$this->assertTrue($result->success, $result->msg);
    }

    public function test_downloadFile_not_exist() {
	$result = $this->downloaderService->downloadFile('https://www.sedoc.pl/images/partners-list.pngasdf');

	$this->assertFalse($result->success, $result->msg);
    }

    public function test_downloadFile_not_image() {
	$result = $this->downloaderService->downloadFile('https://www.sedoc.pl');

	$this->assertFalse($result->success, $result->msg);
    }

    public function test_saveFileToTmp() {

	$wynik = $this->downloaderService->saveFileToTmp('http://pastmo.pl/logo.jpg');
	$this->assertTrue(file_exists($wynik));
    }

    public function test_createRekord() {

	$wynik = $this->downloaderService->createRekord('http://pastmo.pl/logo.jpg',
		dirname(__FILE__) . '\example_file.jpg');
	$this->assertEquals('logo.jpg', $wynik->getName());
	$this->assertEquals('pastmo.pl/logo.jpg', $wynik->getLink());
	$this->assertNotNull($wynik->getContentHash());
    }

    public function test_getLastRecordForLink() {
	$link = date("Y-m-d m:s");

	$file = $this->createFile($link, 42);
	$this->createFile($link, 2);

	$result = $this->downloaderService->getLastRecordForLink($link);

	$this->assertEquals($file->getId(), $result->getId());
    }

    public function test_getFiles() {
	$result = $this->downloaderService->getFiles();

	$this->assertGreaterThan(0, count($result));
    }

    public function test_getLastRecordForLink_no_results() {
	$result = $this->downloaderService->getLastRecordForLink('not existing link');

	$this->assertEquals(0, $result->getVersion());
	$this->assertEquals('', $result->getContentHash());
    }

    public function test_checkFileHasBeenChanged() {
	$file1 = $this->createFile("link", 42);
	$file1->setContentHash("hash1");

	$file2 = $this->createFile("link", 42);
	$file2->setContentHash("other hash");

	$this->assertTrue($this->downloaderService->hasBeenChanged($file1, $file2));
    }

    public function test_setPathAndVersion() {
	$prev = $this->createFile("link", 42);
	$prev->setContentHash("hash1");

	$file = $this->createFile("pastmo.pl/link.jpg", 1);

	$this->downloaderService->setPathAndVersion($prev, $file);

	$this->assertEquals(43, $file->getVersion());
	$this->assertEquals($this->testDir . '/43_pastmo.pl_link.jpg', $file->getPath());
    }

    public function test_removeTmp() {
	try {
	    $this->downloaderService->removeTmp();
	    $this->assertTrue(true);
	} catch (\Excepiton $e) {
	    $this->fail('There shouldent be exception', $e->getMessage());
	}
    }

    private function createFile($link, $version) {
	$file = new Files();
	$file->setLink($link);
	$file->setVersion($version);
	$this->downloaderService->save($file);
	return $file;
    }

    private function createDownloaderMock() {
	$this->downloaderService = $this->getMockBuilder(DownloaderService::class)
			->setConstructorArgs([$this->doctrine])
			->setMethods(['hasBeenChanged'])->getMock();
	$this->downloaderService->method('hasBeenChanged')->willReturn(true);
    }

    private function setTestDirs() {
	$this->downloaderService
		->setTempDir("public/tmp")
		->setUploadDir($this->testDir);
    }

    protected function tearDown() {
	parent::tearDown();

	$this->entityManager->close();
	$this->entityManager = null;
    }

}
